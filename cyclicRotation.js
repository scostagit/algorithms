const solution = (A, K) =>{

    let total = A.length;
    let result = [];

    for(let i = 0; i < total; i ++){
        let index = (K+i)%total;
        result[index] = A[i];
    }

    return result;
}


var arr = [1,2,3,4,5];
console.log(arr);
console.log(solution(arr, 4));