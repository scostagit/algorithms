var arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

const search = (arr, left, right, value)=>{

    if(right >= left){

        let index = parseInt(left + (right - left)/2);
        let current = arr[index];

        console.log(`Reading value ${current}`);

        if(current === value){
            return value;
        }

        if(current > value){
            return search(arr, left, index -1, value);
        }

        return search(arr, index+1, right, value);
    }

    return -1;
};

search(arr, 0, arr.length -1, 4);
