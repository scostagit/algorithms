var arr = [12244,748,874,9824,23,5,1,9,14,93,525,418,7258];

const sort = (arr)=>{
    if(arr.length === 1) return arr;

    var middle = Math.floor(arr.length / 2);

    var left = arr.slice(0, middle);
    var right = arr.slice(middle);

    return merge(sort(left), sort(right));
};

const merge = (left, right)=>{
    var indexLeft = 0;
    var indexRight = 0;
    var result = [];

    while(indexLeft < left.length && indexRight < right.length){

        if(left[indexLeft] < right[indexRight]){
            result.push(left[indexLeft]);
            indexLeft++
        }else{
            result.push(right[indexRight]);
            indexRight ++;
        }

    }

    return result.concat(left.slice(indexLeft)).concat(right.slice(indexRight));

};


console.log(arr);
console.log(sort(arr));