var arr = [0,1,2,3,4,5,6,7,8,9];

const suffler = (arr)=>{

    for(var i =0; i < arr.length; i ++){

        var random = Math.floor(Math.random() * i + 1);

        var current  = arr[i];
        arr[i] = arr[random];
        arr[random] = current;


    }

    return arr;
}

console.log(arr);
console.log(suffler(arr));