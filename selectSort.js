var arr = [12244,748,874,9824,23,5,1,9,14,93,525,418,7258];

const selectSort = (arr)=>{

    var i = 0;

    while(i < arr.length){

        var current = arr[i];
        var next = arr[i+1];

        if(current > next){
            arr[i] = next;
            arr[i+1] = current;
            i = -1;
        }
        i++;
    }

    return arr;
};

console.log(arr);
console.log(selectSort(arr));

