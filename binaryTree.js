var arrTree = {
    left:{
        left:undefined,
        rigth:{
            left:undefined,
            rigth:undefined,
            value:2
        },
        value:3
    },
    rigth:undefined,
    value:10
}

const preOrder= (tree)=>{
    console.log(tree.value);
    tree.left && preOrder(tree.left);
    tree.rigth && preOrder(tree.rigth);
}

const inOrder= (tree)=>{
    tree.left && inOrder(tree.left);
    console.log(tree.value);
    tree.rigth && inOrder(tree.rigth);
}

const posOrder= (tree)=>{
    tree.left && posOrder(tree.left);
    tree.rigth && posOrder(tree.rigth);
    console.log(tree.value);
}

console.log("pre order");
preOrder(arrTree);
console.log("in order")
inOrder(arrTree);
console.log('pos order')
posOrder(arrTree);