const input = [[-3,-2,-1, 1],
               [-2, 2, 3, 4],
               [ 4, 5, 7, 8]]

const countNegativeNumbers = (input)=>{

    let line = 0;
    let column = input[line].length - 1;
    let count = 0;

    while(line < input.length && column >=0 ){

        if(input[line][column] < 0){
            count += column+1;
            line++;
        }else{
            column --;
        }
    }

    return count;

};


console.log(countNegativeNumbers(input));