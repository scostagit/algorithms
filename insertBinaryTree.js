var arrtree = {};

const insert = (tree, value)=>{

    if(tree.value){
        if(value > tree.value){
            insert(tree.right, value);
        }else{
            insert(tree.left, value);
        }
    }else{
        tree.value = value;
        tree.right = {};
        tree.left = {};
    }

};

insert(arrtree, 10);
//.log(tree);
insert(arrtree, 11);
//console.log(tree);
insert(arrtree, 9);
//console.log(tree);
insert(arrtree, 8);
//console.log(tree);

const search = (tree, value)=>{

    if(!tree.value || tree.value === value){
        return tree.value;
    }

    if(value < tree.value){
        return search(tree.left, value);
    }

   return search(tree.right, value);
};

console.log(search(arrtree, 9));