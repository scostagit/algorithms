
const solution = (A)=>{

    let sum1 = 0;
    let sum2 = 0;
    let total = A.length;

    for(let i =0; i< total; i++){
        sum1+= A[i];
        sum2+= i + 1;
    }

    sum2+= total +1;

    return sum2 - sum1;

}

console.log(solution([4,1,3,2,5,6,7,9]));